﻿using System;

namespace Anima {
  public class MpvController : MpvPlayer{
    public MpvController(IntPtr mpvHandle) {
      Initialize(mpvHandle);
    }

    public void Load(string mediaPath) {
      LoadFile(mediaPath);
    }
  }
}