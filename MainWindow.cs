﻿using System.Windows.Forms;

namespace Anima {
  public partial class MainWindow : Form {
    public MainWindow() {
      InitializeComponent();
      OpenFile();
    }

    private void OpenFile() {
      MpvController player = new MpvController(MainView.Handle);
      var path = @"D:\Zona Downloads\[KORSARS]_Plunderer.WEBRip.1080p_[StudioBand]\Plunderer_01_RU_HD.mp4";
      player.Load(path);
    }
  }
}